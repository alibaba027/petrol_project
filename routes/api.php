<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Api\PressureController;
use App\Http\Controllers\admin\DeviceController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/add-pressure',[DeviceController::class,'addPressure']);
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
