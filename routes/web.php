<?php
use App\Http\Controllers\admin\PermissionController;
use App\Http\Controllers\admin\RoleController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\CustomParamController;
use App\Http\Controllers\admin\WellController;
use App\Http\Controllers\admin\AreaController;
use App\Http\Controllers\admin\DeviceController;
use App\Http\Controllers\admin\SettingController;
use App\Http\Controllers\admin\MasterMeterControler;
use App\Http\Controllers\admin\WaterMeterController;
use App\Http\Controllers\admin\DeductionController;
use App\Http\Controllers\admin\TankController;
use App\Http\Controllers\admin\ReportController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login',[UserController::class,'login'])->name('login');
Route::post('/login',[UserController::class,'loginPost'])->name('loginPost');
Route::get('/logout',[UserController::class,'logout']);
Route::get('/',[UserController::class,'login'])->name('login');
Route::middleware([auth::class])->group(function () {
    Route::get('/dashboard', function () {
        // Auth::loginUsingId(1);
        return view('dashboard');
    });
    Route::group(['prefix' => 'admin'], function(){
        Route::group(['prefix' => 'permission'], function(){
            Route::get('/index',       [PermissionController::class,'index']);
            Route::get('/create',      [PermissionController::class,'create']);
            Route::post('/store',      [PermissionController::class,'store']);
            Route::get('/edit/{id}',   [PermissionController::class,'edit']);
            Route::post('/update',     [PermissionController::class,'update']);
            Route::get('/delete/{id}', [PermissionController::class,'delete']);
        });

        Route::group(['prefix' => 'setting'], function(){
            Route::get('/email',     [SettingController::class,'email']);
            Route::get('/create',    [SettingController::class,'sms']);
            Route::post('/email',    [SettingController::class,'emailPost']);
            Route::post('/sms',      [SettingController::class,'smsPost']);
            Route::get('/mail',      [SettingController::class,'testmail']);
            Route::get('/sms1',      [SettingController::class,'smsSend']);
            Route::get('/whats',      [SettingController::class,'whats']);
        });
        
        Route::group(['prefix' => 'role'], function(){
            Route::get('/index',       [RoleController::class,'index']);
            Route::get('/create',      [RoleController::class,'create']);
            Route::post('/store',      [RoleController::class,'store']);
            Route::get('/edit/{id}',   [RoleController::class,'edit']);
            Route::post('/update',     [RoleController::class,'update']);
            Route::get('/delete/{id}', [RoleController::class,'delete']);
        });

        Route::group(['prefix' => 'well'], function(){
            Route::get('/index',       [WellController::class,'index']);
            Route::get('/create',      [WellController::class,'create']);
            Route::post('/store',      [WellController::class,'store']);
            Route::get('/edit/{id}',   [WellController::class,'edit']);
            Route::post('/update',     [WellController::class,'update']);
            Route::get('/delete/{id}', [WellController::class,'delete']);
            Route::get('/add/data/{id}', [WellController::class,'addData']);
            Route::post('/update/data', [WellController::class,'updateData']);
        });

        Route::group(['prefix' => 'master_meter'], function(){
            Route::get('/index',       [MasterMeterControler::class,'index']);
            Route::get('/create',      [MasterMeterControler::class,'create']);
            Route::post('/store',      [MasterMeterControler::class,'store']);
            Route::get('/edit/{id}',   [MasterMeterControler::class,'edit']);
            Route::post('/update',     [MasterMeterControler::class,'update']);
            Route::get('/delete/{id}', [MasterMeterControler::class,'delete']);
            Route::get('/add/data/{id}', [MasterMeterControler::class,'addData']);
            Route::post('/update/data', [MasterMeterControler::class,'updateData']);
        });
        Route::group(['prefix' => 'tank'], function(){
            Route::get('/index',       [TankController::class,'index']);
            Route::get('/create',      [TankController::class,'create']);
            Route::post('/store',      [TankController::class,'store']);
            Route::get('/edit/{id}',   [TankController::class,'edit']);
            Route::post('/update',     [TankController::class,'update']);
            Route::get('/delete/{id}', [TankController::class,'delete']);
            Route::get('/add/data/{id}', [TankController::class,'addData']);
            Route::post('/update/data', [TankController::class,'updateData']);
        });
        Route::group(['prefix' => 'water_meter'], function(){
            Route::get('/index',       [WaterMeterController::class,'index']);
            Route::get('/create',      [WaterMeterController::class,'create']);
            Route::post('/store',      [WaterMeterController::class,'store']);
            Route::get('/edit/{id}',   [WaterMeterController::class,'edit']);
            Route::post('/update',     [WaterMeterController::class,'update']);
            Route::get('/delete/{id}', [WaterMeterController::class,'delete']);
            Route::get('/add/data/{id}', [WaterMeterController::class,'addData']);
            Route::post('/update/data', [WaterMeterController::class,'updateData']);
        });
        Route::group(['prefix' => 'deduction'], function(){
            Route::get('/index',       [DeductionController::class,'index']);
            Route::get('/create',      [DeductionController::class,'create']);
            Route::post('/store',      [DeductionController::class,'store']);
            Route::get('/edit/{id}',   [DeductionController::class,'edit']);
            Route::post('/update',     [DeductionController::class,'update']);
            Route::get('/delete/{id}', [DeductionController::class,'delete']);
            Route::get('/add/data/{id}', [DeductionController::class,'addData']);
            Route::post('/update/data', [DeductionController::class,'updateData']);
        });
        Route::group(['prefix' => 'report/production'], function(){
            Route::get('/index',       [ReportController::class,'productionIndex']);
            Route::get('/{id}',[ReportController::class,'productionReport']);
        });

        Route::group(['prefix' => 'area'], function(){
            Route::get('/index',       [AreaController::class,'index']);
            Route::get('/create',      [AreaController::class,'create']);
            Route::post('/store',      [AreaController::class,'store']);
            Route::get('/edit/{id}',   [AreaController::class,'edit']);
            Route::post('/update',     [AreaController::class,'update']);
            Route::get('/delete/{id}', [AreaController::class,'delete']);
        });

        Route::group(['prefix' => 'device'], function(){
            Route::get('/index',       [DeviceController::class,'index']);
            Route::get('/create',      [DeviceController::class,'create']);
            Route::post('/store',      [DeviceController::class,'store']);
            Route::get('/edit/{id}',   [DeviceController::class,'edit']);
            Route::post('/update',     [DeviceController::class,'update']);
            Route::get('/delete/{id}', [DeviceController::class,'delete']);
            Route::get('data/{id}',[DeviceController::class,'deviceData']);
        });

        Route::group(['prefix' => 'user'], function(){
            Route::get('/index',       [UserController::class,'index']);
            Route::get('/create',      [UserController::class,'create']);
            Route::post('/store',      [UserController::class,'store']);
            Route::get('/edit/{id}',   [UserController::class,'edit']);
            Route::post('/update',     [UserController::class,'update']);
            Route::get('/delete/{id}', [UserController::class,'delete']);
            Route::get('/profile/edit', [UserController::class,'editProfile']);
            Route::post('/update/profile',     [UserController::class,'updateProfile']);
            
        });
        Route::group(['prefix' => '/client/param'], function(){
            Route::get('/index',       [CustomParamController::class,'index']);
            Route::get('/create',      [CustomParamController::class,'create']);
            Route::post('/store',      [CustomParamController::class,'store']);
            Route::get('/edit/{id}',   [CustomParamController::class,'edit']);
            Route::post('/update',     [CustomParamController::class,'update']);
            Route::get('/delete/{id}', [CustomParamController::class,'delete']);
        });

    });
});
Route::group(['prefix' => 'email'], function(){
    Route::get('inbox', function () { return view('pages.email.inbox'); });
    Route::get('read', function () { return view('pages.email.read'); });
    Route::get('compose', function () { return view('pages.email.compose'); });
});

Route::group(['prefix' => 'apps'], function(){
    Route::get('chat', function () { return view('pages.apps.chat'); });
    Route::get('calendar', function () { return view('pages.apps.calendar'); });
});

Route::group(['prefix' => 'ui-components'], function(){
    Route::get('accordion', function () { return view('pages.ui-components.accordion'); });
    Route::get('alerts', function () { return view('pages.ui-components.alerts'); });
    Route::get('badges', function () { return view('pages.ui-components.badges'); });
    Route::get('breadcrumbs', function () { return view('pages.ui-components.breadcrumbs'); });
    Route::get('buttons', function () { return view('pages.ui-components.buttons'); });
    Route::get('button-group', function () { return view('pages.ui-components.button-group'); });
    Route::get('cards', function () { return view('pages.ui-components.cards'); });
    Route::get('carousel', function () { return view('pages.ui-components.carousel'); });
    Route::get('collapse', function () { return view('pages.ui-components.collapse'); });
    Route::get('dropdowns', function () { return view('pages.ui-components.dropdowns'); });
    Route::get('list-group', function () { return view('pages.ui-components.list-group'); });
    Route::get('media-object', function () { return view('pages.ui-components.media-object'); });
    Route::get('modal', function () { return view('pages.ui-components.modal'); });
    Route::get('navs', function () { return view('pages.ui-components.navs'); });
    Route::get('navbar', function () { return view('pages.ui-components.navbar'); });
    Route::get('pagination', function () { return view('pages.ui-components.pagination'); });
    Route::get('popovers', function () { return view('pages.ui-components.popovers'); });
    Route::get('progress', function () { return view('pages.ui-components.progress'); });
    Route::get('scrollbar', function () { return view('pages.ui-components.scrollbar'); });
    Route::get('scrollspy', function () { return view('pages.ui-components.scrollspy'); });
    Route::get('spinners', function () { return view('pages.ui-components.spinners'); });
    Route::get('tabs', function () { return view('pages.ui-components.tabs'); });
    Route::get('tooltips', function () { return view('pages.ui-components.tooltips'); });
});

Route::group(['prefix' => 'advanced-ui'], function(){
    Route::get('cropper', function () { return view('pages.advanced-ui.cropper'); });
    Route::get('owl-carousel', function () { return view('pages.advanced-ui.owl-carousel'); });
    Route::get('sortablejs', function () { return view('pages.advanced-ui.sortablejs'); });
    Route::get('sweet-alert', function () { return view('pages.advanced-ui.sweet-alert'); });
});

Route::group(['prefix' => 'forms'], function(){
    Route::get('basic-elements', function () { return view('pages.forms.basic-elements'); });
    Route::get('advanced-elements', function () { return view('pages.forms.advanced-elements'); });
    Route::get('editors', function () { return view('pages.forms.editors'); });
    Route::get('wizard', function () { return view('pages.forms.wizard'); });
});

Route::group(['prefix' => 'charts'], function(){
    Route::get('apex', function () { return view('pages.charts.apex'); });
    Route::get('chartjs', function () { return view('pages.charts.chartjs'); });
    Route::get('flot', function () { return view('pages.charts.flot'); });
    Route::get('peity', function () { return view('pages.charts.peity'); });
    Route::get('sparkline', function () { return view('pages.charts.sparkline'); });
});

Route::group(['prefix' => 'tables'], function(){
    Route::get('basic-tables', function () { return view('pages.tables.basic-tables'); });
    Route::get('data-table', function () { return view('pages.tables.data-table'); });
});

Route::group(['prefix' => 'icons'], function(){
    Route::get('feather-icons', function () { return view('pages.icons.feather-icons'); });
    Route::get('mdi-icons', function () { return view('pages.icons.mdi-icons'); });
});

Route::group(['prefix' => 'general'], function(){
    Route::get('blank-page', function () { return view('pages.general.blank-page'); });
    Route::get('faq', function () { return view('pages.general.faq'); });
    Route::get('invoice', function () { return view('pages.general.invoice'); });
    Route::get('profile', function () { return view('pages.general.profile'); });
    Route::get('pricing', function () { return view('pages.general.pricing'); });
    Route::get('timeline', function () { return view('pages.general.timeline'); });
});

Route::group(['prefix' => 'auth'], function(){
    Route::get('login', function () { return view('pages.auth.login'); });
    Route::get('register', function () { return view('pages.auth.register'); });
});

Route::group(['prefix' => 'error'], function(){
    Route::get('404', function () { return view('pages.error.404'); });
    Route::get('500', function () { return view('pages.error.500'); });
});

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    
    return "Cache is cleared";
});
Route::get('/migrate', function() {
    Artisan::call('migrate');
    return "Migrated";
});
// 404 for undefined routes
Route::any('/{page?}',function(){
    return View::make('pages.error.404');
})->where('page','.*');
