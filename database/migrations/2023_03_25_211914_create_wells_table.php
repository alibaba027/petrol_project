<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wells', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('name');
            $table->json('param_value');
            // $table->string('tp')->nullable();
            // $table->string('cp')->nullable();
            // $table->string('lp')->nullable();
            // $table->string('gas')->nullable();
            // $table->string('water')->nullable();
            // $table->string('foamer')->nullable();
            // $table->string('chem_use')->nullable();
            // $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wells');
    }
};
