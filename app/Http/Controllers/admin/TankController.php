<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tank;
use App\Area;
use Auth;
use App\CustomParam;
class TankController extends Controller
{
    public function index(){
        $rows=Tank::all();
        $custom_params=CustomParam::where('user_id',Auth::user()->id??1)->get();
        $custom_params=[];
        foreach($rows as $row){
            $params=json_decode($row->params);
            if($params){
                foreach($params as $key=>$value){
                    if(!in_array($key,$custom_params)){
                        array_push($custom_params,$key);
                    }
                }
            }
            
        }
        return view('admin.tanks.index',compact('rows','custom_params'));
    }
    public function create(Request $req){
        $areas=Area::all();
        $customParams=CustomParam::where('user_id',Auth::user()->id??1)->get();
        return view('admin.tanks.add',compact('areas','customParams'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $param_value=null;
         foreach($req->params as $key=>$value){
            $param_value[$value]=0;
         }
        Tank::create([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
            'params'=>json_encode($param_value),
            'user_id'=>Auth::user()->id??1
        ]);
        return redirect('/admin/tank/index')->with('success','Successfully Done');
    }
    public function edit($id){
        $tank=Tank::find($id);
        $areas=Area::all();
        $customParams=CustomParam::where('user_id',Auth::user()->id??1)->get();
        if($tank->params)
            $tankParams=json_decode($tank->params??[]);
        else    
            $tankParams=[];
        $tankParams1=(array)$tankParams;
        $tankParams=[];
        foreach($tankParams1 as $k=>$v){
            array_push($tankParams,$k);
        }
        return view('admin.tanks.add',compact('tank','areas','customParams','tankParams'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         Tank::find($req->id);
        
            $param_value=null;
            foreach($req->params as $key=>$value){
               $param_value[$value]=0;
            }
            Tank::where('id',$req->id)->update([
                'params' =>json_encode($param_value)
            ]);
         
         
            Tank::where('id',$req->id)->update([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
        ]);
        return redirect('/admin/tank/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=Tank::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
    public function addData($id){
        $tank=Tank::find($id);
        return view('admin.tanks.data',compact('tank'));
    }
    public function updateData(Request $req){

        Tank::where('id',$req->id)->update([
            'name' =>$req->name,
            'params'=>json_encode($req->param),
        ]);
        return redirect('/admin/tank/index')->with('success','Successfully Done');
    }
}
