<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Auth,Hash,DB;
class UserController extends Controller
{
    public function login(){
        if(Auth::user())
            return redirect('/dashboard');
        return view('auth.login');
    }
    public function loginPost(Request $req){
        $user=User::where('email',$req->email)->first();
        if(!$user)
            return back()->with('error','Invalid Credencials');
        if(!Hash::check($req->password, $user->password))
            return back()->with('error','Invalid Credencials');
        Auth::login($user);
        return redirect('/dashboard')->with('success','Successfully Logedin');
    }
    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
    public function index(Request $req){
        
        if(in_array('Super Admin',Auth::user()->getRoleNames()->toArray())){
            $rows=User::all();
        }else{
            $rows=User::where('created_by',Auth::user()->id)->get();
        }
        return view('admin.user.index',compact('rows'));
    }
    public function create(Request $req){
        if(in_array('Super Admin',Auth::user()->getRoleNames()->toArray())){
            $roles=Role::all();
        }elseif(in_array('Corporate Admin',Auth::user()->getRoleNames()->toArray())){
            $roles=Role::whereIn('name',['Supervisor','Field Tech'])->get();
        }elseif(in_array('Supervisor',Auth::user()->getRoleNames()->toArray())){
            $roles=Role::whereIn('name',['Field Tech'])->get();
        }
        return view('admin.user.add',compact('roles'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'email'=>'required|unique:users,email'
         ]);

         $user=User::create([
            'name'=>$req->name,
            'email'=>$req->email,
            'password'=>Hash::make($req->password),
            'created_by'=>Auth::user()->id,
            'phone'=>$req->phone??''
         ]);
         $user->assignRole($req->input('role'));
        return redirect('/admin/user/index')->with('success','Added successfully');;
    }
    public function edit($id){
        $user=User::find($id);
        $roles=Role::all();
        return view('admin.user.add',compact('user','roles'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'email'=>'required',
            // 'type'=>'required'
         ]);
         $exist=User::where('email',$req->email)->where('id','<>',$req->id)->first();;
         if($exist)
            return back()->with('error','Already Exist');
         $user=User::find($req->id);
         $user->name=$req->name;
         $user->phone=$req->phone??'';
         if($req->password){
            $user->password= Hash::make($req->password);
         }
         DB::table('model_has_roles')->where('model_id',$req->id)->delete();
         $user->assignRole([$req->input('role')]);
         $user->save();
        return redirect('/admin/user/index')->with('success','Successfully Updated');
    }
    public function delete($id){
        $result=User::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
    public function editProfile(){
        return view('admin.user.profile');
    }
    public function updateProfile(Request $req){
        $user=User::find(Auth::user()->id);
        $user->name=$req->name;
        $user->phone=$req->phone??'';
        if($req->password){
            $user->password=Hash::make($req->password);
        }
        $user->save();
        return redirect('/dashboard')->with('success','Successfully Updated');
    }
}
