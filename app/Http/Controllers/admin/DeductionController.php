<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Deduction;
use App\Area;
use Auth;
use App\CustomParam;
class DeductionController extends Controller
{
    
    public function index(){
        $rows=Deduction::all();
        $custom_params=CustomParam::where('user_id',Auth::user()->id??1)->get();
        $custom_params=[];
        foreach($rows as $row){
            $params=json_decode($row->params);
            if($params){
                foreach($params as $key=>$value){
                    if(!in_array($key,$custom_params)){
                        array_push($custom_params,$key);
                    }
                }
            }
            
        }
        return view('admin.deductions.index',compact('rows','custom_params'));
    }
    public function create(Request $req){
        $areas=Area::all();
        $customParams=CustomParam::where('user_id',Auth::user()->id??1)->get();
        return view('admin.deductions.add',compact('areas','customParams'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $param_value=null;
         foreach($req->params as $key=>$value){
            $param_value[$value]=0;
         }
         Deduction::create([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
            'params'=>json_encode($param_value),
            'user_id'=>Auth::user()->id??1
        ]);
        return redirect('/admin/deduction/index')->with('success','Successfully Done');
    }
    public function edit($id){
        $deduction=Deduction::find($id);
        $areas=Area::all();
        $customParams=CustomParam::where('user_id',Auth::user()->id??1)->get();
        if($deduction->params)
            $deductionParams=json_decode($deduction->params??[]);
        else    
            $deductionParams=[];
        $deductionParams1=(array)$deductionParams;
        $deductionParams=[];
        foreach($deductionParams1 as $k=>$v){
            array_push($deductionParams,$k);
        }
        return view('admin.deductions.add',compact('deduction','areas','customParams','deductionParams'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $well=Deduction::find($req->id);
        
            $param_value=null;
            foreach($req->params as $key=>$value){
               $param_value[$value]=0;
            }
            Deduction::where('id',$req->id)->update([
                'params' =>json_encode($param_value)
            ]);
         
         
            Deduction::where('id',$req->id)->update([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
        ]);
        return redirect('/admin/deduction/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=Deduction::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
    public function addData($id){
        $deduction=Deduction::find($id);
        return view('admin.deductions.data',compact('deduction'));
    }
    public function updateData(Request $req){

        Deduction::where('id',$req->id)->update([
            'name' =>$req->name,
            'params'=>json_encode($req->param),
        ]);
        return redirect('/admin/deduction/index')->with('success','Successfully Done');
    }
}
