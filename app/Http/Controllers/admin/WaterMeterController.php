<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\WaterMeter;
use App\Area;
use Auth;
use App\CustomParam;
class WaterMeterController extends Controller
{
    public function index(){
        $rows=WaterMeter::all();
        $custom_params=CustomParam::where('user_id',Auth::user()->id??1)->get();
        $custom_params=[];
        foreach($rows as $row){
            $params=json_decode($row->params);
            if($params){
                foreach($params as $key=>$value){
                    if(!in_array($key,$custom_params)){
                        array_push($custom_params,$key);
                    }
                }
            }
            
        }
        return view('admin.water_meters.index',compact('rows','custom_params'));
    }
    public function create(Request $req){
        $areas=Area::all();
        $customParams=CustomParam::where('user_id',Auth::user()->id??1)->get();
        return view('admin.water_meters.add',compact('areas','customParams'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $param_value=null;
         foreach($req->params as $key=>$value){
            $param_value[$value]=0;
         }
         WaterMeter::create([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
            'params'=>json_encode($param_value),
            'user_id'=>Auth::user()->id??1
        ]);
        return redirect('/admin/water_meter/index')->with('success','Successfully Done');
    }
    public function edit($id){
        $water_meter=WaterMeter::find($id);
        $areas=Area::all();
        $customParams=CustomParam::where('user_id',Auth::user()->id??1)->get();
        if($water_meter->params)
            $water_meterParams=json_decode($water_meter->params??[]);
        else    
            $water_meterParams=[];
        $water_meterParams1=(array)$water_meterParams;
        $water_meterParams=[];
        foreach($water_meterParams1 as $k=>$v){
            array_push($water_meterParams,$k);
        }
        return view('admin.water_meters.add',compact('water_meter','areas','customParams','water_meterParams'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $well=WaterMeter::find($req->id);
        
            $param_value=null;
            foreach($req->params as $key=>$value){
               $param_value[$value]=0;
            }
            WaterMeter::where('id',$req->id)->update([
                'params' =>json_encode($param_value)
            ]);
         
         
            WaterMeter::where('id',$req->id)->update([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
        ]);
        return redirect('/admin/water_meter/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=WaterMeter::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
    public function addData($id){
        $water_meter=WaterMeter::find($id);
        return view('admin.water_meters.data',compact('water_meter'));
    }
    public function updateData(Request $req){

        WaterMeter::where('id',$req->id)->update([
            'name' =>$req->name,
            'params'=>json_encode($req->param),
        ]);
        return redirect('/admin/water_meter/index')->with('success','Successfully Done');
    }
}
