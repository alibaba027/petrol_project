<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Area;
use App\User;
use App\CustomParam;
use App\Well;
use Auth;
class AreaController extends Controller
{
    public function index(){
        $rows=Area::all();
        if(in_array('Corporate Admin',Auth::user()->getRoleNames()->toArray())){
            $rows=  Area::where('user_id',Auth::user()->id)->get();
        }
        return view('admin.areas.index',compact('rows'));
    }
    public function create(Request $req){
        $params=CustomParam::where('user_id',Auth::user()->id)->get();
        $users = User::role('Corporate Admin')->get();
        return view('admin.areas.add',compact('params','users'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'user_id'=>'required|exists:users,id'
         ]);
         $param_value=null; 
         foreach($req->params as $key=>$value){
            $param_value[$key]=$value;
         }
        Area::create([
            'name' =>$req->name,
            'params'=>json_encode($param_value),
            'user_id'=>$req->user_id
        ]);
        return redirect('/admin/area/index')->with('success','Successfully Done');
    }
    public function edit($id){
        $area=Area::find($id);
        $users = User::role('Corporate Admin')->get();
        $supvisors_users=User::role('Supervisor')->where('created_by',Auth::user()->id)->get();
        $supvisors_users_ids=$supvisors_users->pluck('id')->toArray();
        array_push($supvisors_users_ids,Auth::user()->id);
        $field_tech_users=User::role('Field Tech')->whereIn('created_by',$supvisors_users_ids)->get();
        $customParams=CustomParam::where('user_id',Auth::user()->id)->get();
        $areaParams=json_decode($area->params??[]);
        return view('admin.areas.add',compact('customParams','area','users','supvisors_users','areaParams','field_tech_users'));
    }
    public function update(Request $req){
        // dd($req->all());
        $this->validate($req,[
            'name'=>'required'
         ]);
         $param_value=null;
         if($req->params){
            foreach($req->params as $key=>$value){
                $param_value[$key]=$value;
             }
         }
         $area=Area::where('id',$req->id)->first();
         if(in_array('Super Admin',Auth::user()->getRoleNames()->toArray())){
            $area->name=$req->name;
            $area->user_id=$req->user_id??null;
            
         }
            
        if(in_array('Corporate Admin',Auth::user()->getRoleNames()->toArray())) {
            $area->params=json_encode($param_value);
            $wells=Well::where('area_id',$req->id)->get();
            foreach($wells as $well){
                foreach($param_value as $key=>$value){
                    $well_param_value[$value]=0;
                 }
                 $well->param_value=json_encode($well_param_value);
                 $well->save();
            }
            $area->supervisors()->sync($req->supervisor_ids);
            $area->fieldTechs()->sync($req->feild_tech_ids);
        }        
        $area->save();
        return redirect('/admin/area/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=Area::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
}
