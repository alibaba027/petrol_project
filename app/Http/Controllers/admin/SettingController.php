<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;
use Twilio\Rest\Client;
class SettingController extends Controller
{
    public function email(){
        $setting=Setting::where('name','email_setting')->first();
        $setting=isset($setting->value)?json_decode($setting->value):[];
        return view('admin.setting.email_setting',compact('setting'));
    }
    public function emailPost(Request $req){
        $setting=Setting::where('name','email_setting')->first();
        $data=[
            'email_driver'=>$req->email_driver,
            'email_host'=>$req->email_host,
            'email_port'=>$req->email_port,
            'email_encryption'=>$req->email_encryption,
            'email_username'=>$req->email_username,
            'email_password'=>$req->email_password
        ];
        if(!$setting){
            Setting::create([
                'name'=>'email_setting',
                'value'=>json_encode($data)
            ]);
        }else{
            $setting=new Setting();
            $setting->name='email_setting';
        }
        $setting->value=json_encode($data);
        $setting->save();
        return redirect('/admin/setting/email')->with('success','Successfully Done');
    }
    public function sms(){
        
    }
    public function smsPost(){
        
    }
    public function testmail(){
        \Artisan::call('config:cache');
        \Artisan::call('cache:clear');
        Mail::to('jk@heritageenergy.com')->send(new TestMail());
        if (Mail::flushMacros()) {
            dd(2);
            // return response showing failed emails
        }
        dd(1);
    }
    public function smsSend(){
        $users=User::all();
        foreach($users as $user){
            if($user->phone){
                $receiverNumber = $user->phone;
                $message = "This is a test alert from Full Well Stream Technologies. In the event of a device alarm, you will receive an email and text message detailing the alert. There is no alert at this time.";
                $account_sid = 'ACce01e2997124382cd08b02700f16648a';
                $auth_token = 'e812ecf347ccccc2a1c33470662f349e';
                $twilio_number ='8885172330';
      
                $client = new Client($account_sid, $auth_token);
                $client->messages->create($receiverNumber, [
                    'from' => $twilio_number, 
                    'body' => $message]);
                    echo  $receiverNumber."<br>";

            }

        }
        dd("done");
        $receiverNumber = "+13185609101";
        $message = "This is testing from alert";
  
        try {
  
            $account_sid = 'ACce01e2997124382cd08b02700f16648a';
            $auth_token = 'e812ecf347ccccc2a1c33470662f349e';
            $twilio_number ='8885172330';
  
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number, 
                'body' => $message]);
  
            dd('SMS Sent Successfully.');
  
        } catch (\Exception $e) {
            dd("Error: ". $e->getMessage());
        }
    
    }
    public function whats(){
       
    
        $sid    = "ACe2add7e79da3b0f93ad0362e8006efc7";
        $token  = "1e7daa0aed7a16dc9582c0f753254b50";
        $twilio = new Client($sid, $token);
    
        $message = $twilio->messages
          ->create("whatsapp:+447384586377", // to
            array(
              "from" => "whatsapp:+14155238886",
              "body" => "Test Message twillo test ali here"
            )
          );
    

print($message);
    }
}
