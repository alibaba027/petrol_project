<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MasterMeter;
use App\Area;
use Auth;
use App\CustomParam;
class MasterMeterControler extends Controller
{
   
    public function index(){
        $rows=MasterMeter::all();
        $custom_params=CustomParam::where('user_id',Auth::user()->id??1)->get();
        $custom_params=[];
        foreach($rows as $row){
            $params=json_decode($row->params);
            if($params){
                foreach($params as $key=>$value){
                    if(!in_array($key,$custom_params)){
                        array_push($custom_params,$key);
                    }
                }
            }
            
        }
        return view('admin.master_meters.index',compact('rows','custom_params'));
    }
    public function create(Request $req){
        $areas=Area::all();
        $customParams=CustomParam::where('user_id',Auth::user()->id??1)->get();
        return view('admin.master_meters.add',compact('areas','customParams'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $param_value=null;
         foreach($req->params as $key=>$value){
            $param_value[$value]=0;
         }
        MasterMeter::create([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
            'params'=>json_encode($param_value),
            'user_id'=>Auth::user()->id??1
        ]);
        return redirect('/admin/master_meter/index')->with('success','Successfully Done');
    }
    public function edit($id){
        $master_meter=MasterMeter::find($id);
        $areas=Area::all();
        $customParams=CustomParam::where('user_id',Auth::user()->id??1)->get();
        if($master_meter->params)
            $master_meterParams=json_decode($master_meter->params??[]);
        else    
            $master_meterParams=[];
        $master_meterParams1=(array)$master_meterParams;
        $master_meterParams=[];
        foreach($master_meterParams1 as $k=>$v){
            array_push($master_meterParams,$k);
        }
        return view('admin.master_meters.add',compact('master_meter','areas','customParams','master_meterParams'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $well=MasterMeter::find($req->id);
        
            $param_value=null;
            foreach($req->params as $key=>$value){
               $param_value[$value]=0;
            }
            MasterMeter::where('id',$req->id)->update([
                'params' =>json_encode($param_value)
            ]);
         
         
         MasterMeter::where('id',$req->id)->update([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
        ]);
        return redirect('/admin/master_meter/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=MasterMeter::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
    public function addData($id){
        $master_meter=MasterMeter::find($id);
        return view('admin.master_meters.data',compact('master_meter'));
    }
    public function updateData(Request $req){

        MasterMeter::where('id',$req->id)->update([
            'name' =>$req->name,
            'params'=>json_encode($req->param),
        ]);
        return redirect('/admin/master_meter/index')->with('success','Successfully Done');
    }
}
