<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Device;
use App\Pressure;
use App\User;
use App\Area;
use Auth,DB;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\AlertEmail;
class DeviceController extends Controller
{
    public function index(){
        if(in_array('Super Admin',Auth::user()->getRoleNames()->toArray()))
            $rows=Device::all();
        else
        $rows=Device::where('user_id',Auth::user()->id)->get();
        if(in_array('Supervisor',Auth::user()->getRoleNames()->toArray())){
            $areas=DB::table('area_supervisor')->where('user_id',Auth::user()->id)->get()->pluck('area_id');
            $area_ids=$areas?$areas->toArray():[];
            $rows=Device::whereIn('area_id',$area_ids)->get();
        }
        if(in_array('Field Tech',Auth::user()->getRoleNames()->toArray())){
            $areas=DB::table('area_field_tech')->where('user_id',Auth::user()->id)->get()->pluck('area_id');
            $area_ids=$areas?$areas->toArray():[];
            $rows=Device::whereIn('area_id',$area_ids)->get();
        }
        return view('admin.devices.index',compact('rows'));
    }
    public function create(Request $req){
        $users=User::all();
        $areas=Area::all();
        return view('admin.devices.add',compact('users','areas'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'device_id'=>'required|unique:devices,device_id',
            'type'=>'required',
            'area_id'=>'required'
         ]);
        Device::create([
            'device_id' =>$req->device_id,
            'user_id'=>$req->user_id,
            'type'=>$req->type,
            'area_id'=>$req->area_id,
            'enable_alarm'=>$req->enable_alarm,
            'max_point_psi'=>$req->max_point_psi
        ]);
        return redirect('/admin/device/index')->with('success','Successfully Added');
    }
    public function edit($id){
        $device=Device::find($id);
        $users=User::all();
        $areas=Area::all();
        return view('admin.devices.add',compact('users','device','areas'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'device_id'=>'required',
            'type'=>'required',
            'area_id'=>'required'
         ]);
         $exist=Device::where('device_id',$req->device_id)->where('id','<>',$req->id)->first();
         if($exist)
            return back()->with('error','Already Exist');
         Device::where('id',$req->id)->update([
            'device_id' =>$req->device_id,
            'user_id'=>$req->user_id,
            'type'=>$req->type,
            'area_id'=>$req->area_id,
            'enable_alarm'=>$req->enable_alarm??0,
            'max_point_psi'=>$req->max_point_psi
        ]);
        return redirect('/admin/device/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=Device::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
    public function deviceData($id){
        $device=Device::find($id);
        $time_data=[];
        $betry=Pressure::where('device_id',$id)->orderBy('created_at', 'DESC')->first();
        $data=Pressure::where('device_id',$id)->orderBy('created_at', 'DESC')->take(100)->pluck('pressure');
        $time=Pressure::where('device_id',$id)->orderBy('created_at', 'DESC')->take(100)->pluck('created_at');
        foreach($time as $t){
           
            array_push($time_data,$t->format('U')-18000);
        }
        
        // foreach($time as $t){
        //     $d=str_replace(' ','-',$t->toString());
        //     $d=str_replace(':','-',$d);
        //     array_push($time_data,$d);
        // }
        // dd($time_data);
        $time_data=json_encode($time_data);
        return view('admin.devices.data',compact('data','time_data','device','betry'));
    }
    public function addPressure(Request $req){
        // $2y$10$zLbLcTVBChGIcGJF6pZAFeBAUiyCtkQMEcED/3K3YmaVWWXM8n036
        // dd(Hash::make('ADD-PRESSURE-DATA'));
         $rules = [
            'device_id'=>'required|exists:devices,device_id',
            'pressure'=>'required|numeric',
            'accessToken'=>'required'
        ];
        $validator = \Validator::make($req->all(), $rules);
        if ($validator->fails()) {
            return response()->json([ 'message' => [__('Invalid Request.')]], 400);
        }
        if($req->accessToken !='$2y$10$zLbLcTVBChGIcGJF6pZAFeBAUiyCtkQMEcED/3K3YmaVWWXM8n036')
            return response()->json([ 'message' => [__('Invalid Access Key.')]], 400);
        $device=Device::where('device_id',$req->device_id)->first();
        Pressure::create([
            'device_id'=>$device->id,
            'pressure'=>$req->pressure,
            'unit'=>'PSI',
            'battery_voltage'=>$req->battery_voltage??''
        ]);
        if($device->type=='LPM-100' && $device->enable_alarm && $device->max_point_psi<$req->pressure){
            $data['device']=$device;
            $data['now_psi_point']=$req->pressure;
            Mail::to($device->user->email)->send(new AlertEmail($data));

            if($device->user->phone){
                $receiverNumber = $device->user->phone;
                $message = "New Alert: ". $device->device_id." Line Pressure Threshold Exceeded.Current Line Pressure: ". $req->pressure ." PSI";

                    $account_sid = 'ACce01e2997124382cd08b02700f16648a';
                    $auth_token = 'e812ecf347ccccc2a1c33470662f349e';
                    $twilio_number ='8885172330';

                    $client = new Client($account_sid, $auth_token);
                    $client->messages->create($receiverNumber, [
                        'from' => $twilio_number, 
                        'body' => $message]);
            }
            
            // Mail::to('arkmuzikllc@gmail.com')->send(new AlertEmail($data));
            // Mail::to('loopers1199@gmail.com')->send(new AlertEmail($data));
        }
        return response()->json([ 'message' => [__('Data Added Successfully.')]], 200);
    }
}
