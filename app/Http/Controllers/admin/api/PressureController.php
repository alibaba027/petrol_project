<?php

namespace App\Http\Controllers\admin\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Device;
use App\Pressure;
use Hash,DB;
class PressureController extends Controller
{
    public function addPressure(Request $req){
        // $2y$10$zLbLcTVBChGIcGJF6pZAFeBAUiyCtkQMEcED/3K3YmaVWWXM8n036
        // dd(Hash::make('ADD-PRESSURE-DATA'));
         $rules = [
            'device_id'=>'required|exists:devices,device_id',
            'pressure'=>'required|numeric',
            'accessToken'=>'required'
        ];
        $validator = \Validator::make($req->all(), $rules);
        if ($validator->fails()) {
            return response()->json([ 'message' => [__('Invalid Request.')]], 400);
        }
        if($req->accessToken !='$2y$10$zLbLcTVBChGIcGJF6pZAFeBAUiyCtkQMEcED/3K3YmaVWWXM8n036')
            return response()->json([ 'message' => [__('Invalid Access Key.')]], 400);
        $device=Device::where('device_id',$req->device_id)->first();
        Pressure::create([
            'device_id'=>$device->id,
            'pressure'=>$req->pressure,
            'unit'=>'PSI'
        ]);
        return response()->json([ 'message' => [__('Data Added Successfully.')]], 200);
    }
}
