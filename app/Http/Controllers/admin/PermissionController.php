<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
class PermissionController extends Controller
{
    public function index(Request $req){
        $rows=Permission::all();
        return view('admin.permissions.index',compact('rows'));
    }
    public function create(Request $req){
    
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required|unique:permissions,name'
         ]);
        Permission::create(['name' => $req->name]);
        return back()->with('success','Successfully Done');
    }
    public function edit($id){
        return Permission::find($id);
    }
    public function update(Request $req){
        $exist=Permission::where('name',$req->name)->where('id','<>',$req->id)->first();
        if($exist){
            return back()->with('error','Already Exist');
        }
        Permission::where('id',$req->id)->update(['name'=>$req->name]);
        return back()->with('success','Successfully Done');
    }
    public function delete($id){
        $result=Permission::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;

    }
}
