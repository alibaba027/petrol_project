<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Well;
use App\Area;
use App\CustomParam;
use App\WellData;
use Auth;
class WellController extends Controller
{
    public function index(){
        $rows=Well::all();
        $custom_params=CustomParam::where('user_id',Auth::user()->id??1)->get();
        return view('admin.wells.index',compact('rows','custom_params'));
    }
    public function create(Request $req){
        $custom_params=CustomParam::where('user_id',Auth::user()->id??1)->get();
        $areas=Area::all();
        return view('admin.wells.add',compact('custom_params','areas'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $param_value=null;
         $params=Area::find($req->area_id);
         foreach(json_decode($params->params) as $key=>$value){
            $param_value[$value]=0;
         }
        Well::create([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
            'param_value'=>json_encode($param_value),
            'user_id'=>Auth::user()->id??1
        ]);
        return redirect('/admin/well/index')->with('success','Successfully Done');
    }
    public function edit($id){
        $well=Well::find($id);
        $areas=Area::all();
        $custom_params=CustomParam::where('user_id',Auth::user()->id??1)->get();
        return view('admin.wells.add',compact('custom_params','well','areas'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'name'=>'required',
            'area_id'=>'required'
         ]);
         $area=Area::find($req->area_id);
         $well=Well::find($req->id);
         if($req->area_id!=$well->area_id){
            $param_value=null;
            foreach(json_decode($area->params) as $key=>$value){
               $param_value[$value]=0;
            }
            Well::where('id',$req->id)->update([
                'param_value' =>json_encode($param_value)
            ]);
         }
         
         Well::where('id',$req->id)->update([
            'name' =>$req->name,
            'area_id'=>$req->area_id,
        ]);
        return redirect('/admin/well/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=Well::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
    public function addData($id){
        $well=Well::find($id);
        $data= WellData::where('well_id',$id)->get();
        return view('admin.wells.data',compact('well','data'));
    }
    public function updateData(Request $req){

        WellData::create([
            'well_id' =>$req->id,
            'data'=>json_encode($req->param),
        ]);
        return redirect('/admin/well/index')->with('success','Successfully Done');
    }
}
