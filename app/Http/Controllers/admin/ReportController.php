<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Area;
use App\MasterMeter;
use App\WaterMeter;
use App\Well;
use App\WellData;
class ReportController extends Controller
{
    public function productionIndex(){
        $rows=Area::all();
        return view('admin.reports.production_index',compact('rows'));
    }
    public function productionReport(Request $req,$id){
        // dd($req->all());
        $area=Area::find($id);
        $wells=Well::where('area_id',$id)->get();
        $master_meters=MasterMeter::where('area_id',$id)->get();
        $water_meters=WaterMeter::where('area_id',$id)->get();
        $well_params=[];
        $well_report=[];
        $master_meter_params=[];
        $master_meter_report=[];
        $water_meter_params=[];
        $water_meter_report=[];
        foreach($wells as $row){
            $params=json_decode($row->param_value);
            if($params){
                foreach($params as $key=>$value){
                    if(!in_array($key,$well_params)){
                        array_push($well_params,$key);
                        $well_report[$key]=0;
                    }
                }
            }
            
        }
        $date=$req->date??date('Y-m-d');
        $w_r=['GAS','WATER'];
         foreach($wells as $row){
            $well_data=WellData::where('well_id',$row->id)->whereDate('created_at',$date)->get();
            $current=[];
            if(count($well_data)){
                
                foreach($well_data as $d){
                    $params=json_decode($d->data);
                    foreach($params as $key=>$value){
                        // dd($key,$w_r);
                        if(in_array($key,$w_r)){
                            $well_report[$key]+=$value;
                            if(isset($current[$key])){
                                $current[$key]+=$value;
                            }else{
                                $current[$key]=$value;
                            }
                            
                        }else{
                            $well_report[$key]=0;
                            $current[$key]=0;
                        }
                        
                    }
                }
            }
            $row->param_value=json_encode($current);
            
            // if($params){
            //     foreach($params as $key=>$value){
            //         // dd($key,$w_r);
            //         if(in_array($key,$w_r)){
            //             $well_report[$key]+=$value;
            //         }else{
            //             $well_report[$key]=0;
            //         }
                    
            //     }
            // }
            // dd($well_report);
           
            // if(1){
            //     $well_data=WellData::where('well_id',$row->id)->whereDate('created_at',$date)->get();
            //     if(count($well_data)){
            //         foreach($well_data as $data){
            //             $params=json_decode($data->data);
            //             if($params){
            //                 foreach($params as $key=>$value){
            //                     // dd($key,$w_r);
            //                     if(in_array($key,$w_r)){
            //                         $well_report[$key]+=$value;
            //                     }else{
            //                         $well_report[$key]=0;
            //                     }
                                
            //                 }
            //             }
            //         }
                    
            //     }
              
            // }
        
           
        } 
// dd($wells);
        //master meter
        foreach($master_meters as $row){
            $params=json_decode($row->params);
            if($params){
                foreach($params as $key=>$value){
                    if(!in_array($key,$master_meter_params)){
                        array_push($master_meter_params,$key);
                        $master_meter_report[$key]=0;
                    }
                }
            }
            
        }

        $m_r=['GAS'];
         foreach($master_meters as $row){
            $params=json_decode($row->params);
            if($params){
                foreach($params as $key=>$value){
                    // dd($key,$w_r);
                    if(in_array($key,$m_r)){
                        $master_meter_report[$key]+=$value;
                    }else{
                        $master_meter_report[$key]=0;
                    }
                    
                }
            }
            
        } 

        // water meter
        foreach($water_meters as $row){
            $params=json_decode($row->params);
            if($params){
                foreach($params as $key=>$value){
                    if(!in_array($key,$water_meter_params)){
                        array_push($water_meter_params,$key);
                        $water_meter_report[$key]=0;
                    }
                }
            }
            
        }

        $w_r=['WATER'];
         foreach($water_meters as $row){
            $params=json_decode($row->params);
            if($params){
                foreach($params as $key=>$value){
                    // dd($key,$w_r);
                    if(in_array($key,$w_r)){
                        $water_meter_report[$key]+=$value;
                    }else{
                        $water_meter_report[$key]=0;
                    }
                    
                }
            }
            
        }
        // dd($well_params,$wells,$well_report);
        return view('admin.reports.production_report',compact('area','well_params','wells','well_report','master_meter_report','master_meter_params','master_meters','water_meter_report','water_meter_params','water_meters'));
    }
}
