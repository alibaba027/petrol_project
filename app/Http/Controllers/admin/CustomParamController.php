<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomParam;
use Auth;
class CustomParamController extends Controller
{
    public function index(){
        $rows=CustomParam::where('user_id',Auth::user()->id)->get();
        return view('admin.custom-param.index',compact('rows'));
    }
    public function create(Request $req){
        return view('admin.custom-param.add');
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required',
            // 'type'=>'required'
         ]);
        CustomParam::create([
            'name' =>$req->name,
            // 'type'=>$req->type,
            'user_id'=>Auth::user()->id??1
        ]);
        return redirect('/admin/client/param/index')->with('success','Successfully Done');
    }
    public function edit($id){
        $param=CustomParam::find($id);
        return view('admin.custom-param.add',compact('param'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'name'=>'required',
            // 'type'=>'required'
         ]);
         CustomParam::where('id',$req->id)->update([
            'name' =>$req->name,
            // 'type'=>$req->type,
        ]);
        return redirect('/admin/client/param/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=CustomParam::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
}
