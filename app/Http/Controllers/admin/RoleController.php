<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
class RoleController extends Controller
{
    public function index(Request $req){
        $rows=Role::all();
        return view('admin.roles.index',compact('rows'));
    }
    public function create(Request $req){
        $permissions=Permission::all();
        return view('admin.roles.add',compact('permissions'));
    }
    public function store(Request $req){
        $this->validate($req,[
            'name'=>'required|unique:roles,name'
         ]);
        $role = Role::create(['name' =>$req->name]);
        $role->givePermissionTo($req->permissions);
        return redirect('/admin/role/index')->with('success','Successfully Done');
    }
    public function edit($id){
        $role=Role::find($id);
        $permissions=Permission::all();
        return view('admin.roles.add',compact('permissions','role'));
    }
    public function update(Request $req){
        $this->validate($req,[
            'name'=>'required'
         ]);
         $exist=Role::where('name',$req->name)->where('id','<>',$req->id)->first();
        if($exist){
            return back()->with('error','Already Exist');
        }
        // dd($req->permissions);
        Role::where('id',$req->id)->update(['name' =>$req->name]);
        $role =Role::find($req->id);
        
        DB::table('role_has_permissions')->where('role_id',$req->id)->delete();
        $role->givePermissionTo($req->permissions);
        \Artisan::call('cache:forget spatie.permission.cache');
        \Artisan::call('cache:clear');
        return redirect('/admin/role/index')->with('success','Successfully Done');
    }
    public function delete($id){
        $result=Role::where('id',$id)->delete();
        if($result){
            $data['status']=1;
            $data['id']=$id;
            $data['msg']='Deleted Successfully';
        }else{
            $data['status']=1;
            $data['msg']='Error';
        }
        return $data;
    }
}
