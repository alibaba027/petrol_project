<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pressure extends Model
{
    use HasFactory;
    protected $table='pressure_data';
    protected $fillable=['device_id','pressure','unit','battery_voltage'];
}
