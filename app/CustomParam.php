<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomParam extends Model
{
    use HasFactory;
    protected $table='custom_params';
    protected $fillable=['user_id','name'];
}
