<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deduction extends Model
{
    use HasFactory;
    protected $fillable=['name','params','user_id','area_id'
];
    public function area(){
        return $this->hasOne(Area::class,'id','area_id');
    }

}
