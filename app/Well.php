<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Well extends Model
{
    use HasFactory;
    protected $fillable=['name','param_value','user_id','area_id'
    // ,'tp','cp','lp','gas','water','chem_use','remarks'
];
    public function area(){
        return $this->hasOne(Area::class,'id','area_id');
    }
    public function data(){
        return $this->hasMany(WellData::class,'well_id','id');
    }
}
