<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;
    protected $fillable=['name','sub_name','params','user_id','supervisor_id','field_tech_id'];
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function supervisor(){
        return $this->hasOne(User::class,'id','supervisor_id');
    }
    public function supervisors()
    {
        return $this->belongsToMany(User::class, 'area_supervisor');
    }
    public function fieldTechs()
    {
        return $this->belongsToMany(User::class, 'area_field_tech');
    }
}
