<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WellData extends Model
{
    use HasFactory;
    protected $table='well_data';
    protected $fillable=['well_id','data'];
}
