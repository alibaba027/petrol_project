<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasFactory;
    protected $table='devices';
    protected $fillable=['device_id','user_id','type','area_id','enable_alarm','max_point_psi'];
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function area(){
        return $this->hasOne(Area::class,'id','area_id');
    }
}
