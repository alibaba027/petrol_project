@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Permissions</a></li>
      <li class="breadcrumb-item active" aria-current="page">All Permissions</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Permissions
            <button class="btn btn-sm btn-inverse-secondary"  data-bs-toggle="modal" data-bs-target="#addPermission" style="float: right">Add Permisison</button>
          </h6>
          <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>PERMISSION NAME</th>
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($rows as $key=>$value)
                  <tr id="itemRow_{{ $value->id }}">
                    <th>{{ $key+1 }}</th>
                    <td>{{ $value->name }}</td>
                    <td>
                        <i class="icon feather icon-edit editPermission" data-id="{{ $value->id }}" data-bs-toggle="modal" data-bs-target="#editPermission"></i>&nbsp;&nbsp; 
                        <i class="icon feather icon-trash-2 deleteModel" data-url="/admin/permission/delete/{{ $value->id }}" data-bs-toggle="modal" data-bs-target="#deleteItem"></i> 
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="editPermission" tabindex="-1" aria-labelledby="editPermissionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editPermissionLabel">Edit Permission</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="btn-close"></button>
        </div>
        <form class="forms-sample" action="{{ url('/admin/permission/update') }}" method="post">
            <div class="modal-body">
                @csrf
                <div class="mb-3">
                    <input type="hidden" name="id" id="editId">
                    <label for="exampleInputUsername1" class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" autofocus id="name" autocomplete="off" placeholder="Name">
                </div>      
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-inverse-success">Update</button>
            </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="addPermission" tabindex="-1" aria-labelledby="addPermissionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addPermissionLabel">Add Permission</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="btn-close"></button>
        </div>
        <form class="forms-sample" action="{{ url('/admin/permission/store') }}" method="post">
            <div class="modal-body">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputUsername1" class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name">
                </div>      
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-inverse-success">Add</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
  <script>
    $('.editPermission').click(function(){
        var id=$(this).data('id');
        $.ajax({url: '/admin/permission/edit/'+id,
        success: function(result){
        if(result){
            $('#editId').val(id);
            $('#name').val(result.name);
        }else{
            toastr.error('Not Found');
        }
        }});
    })
  </script>
@endpush