@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
  
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Well Fields</a></li>
     <li class="breadcrumb-item active" aria-current="page">All Well Fields</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">WELL FIELDS
            @if(Auth::user()->can('Add Well Custom Param')) <a href="/admin/client/param/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Field</a>@endif
          </h6>
          <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>FIELD NAME</th>
                    {{-- <th>PARAM TYPE</th> --}}
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($rows as $key=>$value)
                  <tr id="itemRow_{{ $value->id }}">
                    <th>{{ $key+1 }}</th>
                    <td>{{ $value->name }}</td>
                    {{-- <td>{{ $value->type }}</td>      --}}
                    <td>
                      @if(Auth::user()->can('Edit Well Custom Param'))
                        <a href="/admin/client/param/edit/{{ $value->id }}"><i class="icon feather icon-edit"></i></a>&nbsp;&nbsp; 
                       @endif
                        @if(Auth::user()->can('Delete Well Custom Param'))
                        <i class="icon feather icon-trash-2 deleteModel" data-url="/admin/client/param/delete/{{ $value->id }}" data-bs-toggle="modal" data-bs-target="#deleteItem"></i> 
                      @endif
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
  
@endpush