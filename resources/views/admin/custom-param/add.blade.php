@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Well Field</a></li>
      <li class="breadcrumb-item active" aria-current="page">Add Field</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($param->id)) action="{{ url('/admin/client/param/update') }}" @else action="{{ url('/admin/client/param/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($param->id)) Edit Field @else Add Field @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($param->id)) 
                <input type="hidden" name="id" value="{{ $param->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $param->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name">
            </div> 
            {{-- <div class="mb-3">
                <label class="form-label">Type</label>
                <input type="text" class="form-control" name="type" value="{{ $param->type??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Type">
              </div>   --}}
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush