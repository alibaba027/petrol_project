@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">User</a></li>
      <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
    </ol>
  </nav>

  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" action="{{ url('/admin/user/update/profile') }}">
                @csrf
          <h6 class="card-title">Edit Profile
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($user->id))
                <input type="hidden" name="id" value="{{ $user->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ Auth::user()->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" name="email" value="{{ Auth::user()->email??'' }}" @if(isset(Auth::user()->email)) readonly @endif  id="exampleInputUsername1" autocomplete="off" placeholder="Email" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Phone</label>
                <input type="text" class="form-control" name="phone" value="{{ Auth::user()->phone??'' }}"  id="exampleInputUsername1" autocomplete="off" placeholder="+44987987987987" >
            </div>
            <div class="mb-3">
                <label class="form-label">Change Password</label>
                <input type="text" class="form-control" name="password" value=""  id="exampleInputUsername1" autocomplete="off" placeholder="Password" @if(!isset($user->id)) required @endif>
            </div>
            
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush
