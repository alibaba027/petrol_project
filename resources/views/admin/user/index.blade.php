@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />

@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Users</a></li>
      <li class="breadcrumb-item active" aria-current="page">All Users</li>
    </ol>
  </nav>

  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">USERS
            <a href="/admin/user/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add User</a>
          </h6>
          <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th>PHONE</th>
                    <th>ROLE</th>
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($rows as $key=>$value)
                  <tr id="itemRow_{{ $value->id }}">
                    <th>{{ $key+1 }}</th>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ $value->phone??'' }}</td>
                    <td>@foreach($value->getRoleNames() as $name)<span class="badge bg-success"> {{ ucfirst($name) }} </span>@endforeach</td>
                    <td>
                        <a href="/admin/user/edit/{{ $value->id }}"><i class="icon feather icon-edit"></i></a>&nbsp;&nbsp;
                        @if(!in_array('Super Admin',$value->getRoleNames()->toArray()))
                        <i class="icon feather icon-trash-2 deleteModel" data-url="/admin/user/delete/{{ $value->id }}" data-bs-toggle="modal" data-bs-target="#deleteItem"></i>
                        @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>

@endpush
