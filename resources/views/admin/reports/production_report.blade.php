@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
  
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Production Reports</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Area of {{ $area->name }}
           {{-- @if(Auth::user()->can('Add Well')) --}}
            {{-- <a href="/admin/tank/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Tank</a> --}}
            {{-- @endif --}}
            <form action="{{ url('/admin/report/production/'.$area->id) }}" method="get">
            <input type="date" value="<?php echo date('Y-m-d'); ?>" name="date" class="form-control" style="width: 20%;display: inline-block;margin-top: 25px;">
            <button type="submit" class="btn btn-success">Search</button>
          </h6>
          <div class="table-responsive" style="width: 100%;">
              <table class="table">
                <h6>Wells Report</h6>
                <thead>
                  <tr>
                    <th>NAME</th>
                    @foreach($well_params as $well)
                    <th>{{ $well }}</th>
                    @endforeach
                  </tr>
                </thead>
                <tbody>
                    {{-- foreach($wells as $row){
                        $params=json_decode($row->param_value);
                        if($params){
                            foreach($params as $key=>$value){
                                $well_report[$key]+=$value;
                            }
                        }
                        
                    } --}}
                    @foreach($wells as $well)
                    <tr>
                    <td>{{ $well->name }}</td>
                    @php 
                    $params=json_decode($well->param_value);
                    $params=(array)$params;
                    
                    @endphp
                    @foreach($well_params as $well)
                    @php

                    @endphp
                    <th>{{$params[$well]??0 }}</th>
                    @endforeach
                    </tr>
                    @endforeach
                    <tr style="border-bottom: 2px solid;
                    border-top: 2px solid;">
                        <td style="font-weight: bold;">TOTAL</td>
                        @foreach($well_report as $total)
                        <td>@if($total>0){{ $total }} @endif</td>
                        @endforeach
                    </tr>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h6 class="card-title">Mater Meter Report
             {{-- @if(Auth::user()->can('Add Well')) --}}
              {{-- <a href="/admin/tank/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Tank</a> --}}
              {{-- @endif --}}
            </h6>
            <div class="table-responsive" style="width: 100%;">
                <table class="table">
                 
                  <thead>
                    <tr>
                      <th>NAME</th>
                      @foreach($master_meter_params as $master)
                      <th>{{ $master }}</th>
                      @endforeach
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($master_meters as $well)
                      <tr>
                      <td>{{ $well->name }}</td>
                      @php 
                      $params=json_decode($well->params);
                      $params=(array)$params;
                      
                      @endphp
                      @foreach($master_meter_params as $well)
                      @php
  
                      @endphp
                      <th>{{$params[$well]??0 }}</th>
                      @endforeach
                      </tr>
                      @endforeach
                      <tr style="border-bottom: 2px solid;
                      border-top: 2px solid;">
                          <td style="font-weight: bold;">TOTAL</td>
                          @foreach($master_meter_report as $total)
                          <td>@if($total>0){{ $total }} @endif</td>
                          @endforeach
                      </tr>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h6 class="card-title">Water Meter Report
             {{-- @if(Auth::user()->can('Add Well')) --}}
              {{-- <a href="/admin/tank/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Tank</a> --}}
              {{-- @endif --}}
            </h6>
            <div class="table-responsive" style="width: 100%;">
                <table class="table">
                 
                  <thead>
                    <tr>
                      <th>NAME</th>
                      @foreach($water_meter_params as $master)
                      <th>{{ $master }}</th>
                      @endforeach
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($water_meters as $well)
                      <tr>
                      <td>{{ $well->name }}</td>
                      @php 
                      $params=json_decode($well->params);
                      $params=(array)$params;
                      
                      @endphp
                      @foreach($water_meter_params as $well)
                      @php
    
                      @endphp
                      <th>{{$params[$well]??0 }}</th>
                      @endforeach
                      </tr>
                      @endforeach
                      <tr style="border-bottom: 2px solid;
                      border-top: 2px solid;">
                          <td style="font-weight: bold;">TOTAL</td>
                          @foreach($water_meter_report as $total)
                          <td>@if($total>0){{ $total }} @endif</td>
                          @endforeach
                      </tr>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
  </div>

  
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
  
@endpush