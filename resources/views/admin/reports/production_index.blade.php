@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
  
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Production Reports</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Areas
           {{-- @if(Auth::user()->can('Add Well')) --}}
            {{-- <a href="/admin/tank/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Tank</a> --}}
            {{-- @endif --}}
          </h6>
          <div class="table-responsive" style="width: 100%;">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>AREA NAME</th>
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($rows as $key=>$value)
                  <tr id="itemRow_{{ $value->id }}">
                    <th>{{ $key+1 }}</th>
                    <td>{{ $value->name }}</td>
                    
                    <td>
                      {{-- @if(Auth::user()->can('Edit Well')) --}}
                        <a href="/admin/report/production/{{ $value->id }}">View Report</a>
                    {{-- @endif --}}
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
  
@endpush