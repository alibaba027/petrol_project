@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Well</a></li>
      <li class="breadcrumb-item active" aria-current="page">Add/Edit Well</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($well->id)) action="{{ url('/admin/well/update') }}" @else action="{{ url('/admin/well/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($well->id)) Edit Well @else Add Well @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($well->id)) 
                <input type="hidden" name="id" value="{{ $well->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $well->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name">
            </div>
            <div class="mb-3">
              <label for="exampleInputUsername1" class="form-label">Area</label>
              <select class="form-control" name="area_id" required>
                <option value="">--select user--</option>
                @foreach($areas as $area)
                <option @if(isset($well) && $well->area && $well->area->id==$area->id) selected @endif value="{{$area->id}}">{{$area->name}}</option>
                @endforeach
               </select>
          </div>  
            {{-- @php 
            $param_value='';
            $param_value=json_decode($well->param_value??'');
            $param_value=(array)$param_value;
            @endphp
            @foreach($custom_params as $param)
            <div class="mb-3">
                <label class="form-label">{{ $param->name }}</label>
                <input type="text" class="form-control" name="param[{{ $param->name }}]" value="{{ $param_value[$param->name]??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="{{ $param->name  }}">
            </div>   --}}
            {{-- @endforeach --}}
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush