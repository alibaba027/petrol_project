@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Well</a></li>
      <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($well->id)) action="{{ url('/admin/well/update/data') }}" @else action="{{ url('/admin/well/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($well->id)) Edit Data @else Add Data @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($well->id)) 
                <input type="hidden" name="id" value="{{ $well->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $well->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name" readonly>
            </div>  
            @php 
            $param_value='';
            $param_value=json_decode($well->param_value??'');
            $param_value=(array)$param_value;
            @endphp
            @foreach($param_value as $key=>$param)
            <div class="mb-3">
                <label class="form-label">{{ $key }}</label>
                <input type="text" class="form-control" name="param[{{$key}}]" value="" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="{{ $key  }}">
            </div>   
             @endforeach
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Data
           {{-- @if(Auth::user()->can('Add Well')) --}}
            {{-- <a href="/admin/tank/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Tank</a> --}}
            {{-- @endif --}}
          </h6>
          <div class="table-responsive" style="width: 100%;">
              <table class="table">
               
                <thead>
                  <tr>
                    <th>DATE</th>
                    @foreach($param_value as  $key=>$param)
                    <th>{{ $key }}</th>
                    @endforeach
                  </tr>
                </thead>
                <tbody>
                    @foreach($data as $da)
                    @php
                    $dat=json_decode($da->data);
                    @endphp
                    <tr>
                      <td>{{ $da->created_at }}</td>
                      @foreach($dat as $d)
                    
                    <th>{{$d??0 }}</th>
                    @endforeach
                    </tr>
                    @endforeach
                    
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush