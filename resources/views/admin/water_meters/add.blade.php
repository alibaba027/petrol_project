@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Water Meter</a></li>
      <li class="breadcrumb-item active" aria-current="page">Add/Edit Water Meter</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($water_meter->id)) action="{{ url('/admin/water_meter/update') }}" @else action="{{ url('/admin/water_meter/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($water_meter->id)) Edit Water Meter @else Add Water Meter @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($water_meter->id)) 
                <input type="hidden" name="id" value="{{ $water_meter->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $water_meter->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name">
            </div>
            <div class="mb-3">
              <label for="exampleInputUsername1" class="form-label">Area</label>
              <select class="form-control" name="area_id" required>
                <option value="">--select user--</option>
                @foreach($areas as $area)
                <option @if(isset($water_meter) && $water_meter->area && $water_meter->area->id==$area->id) selected @endif value="{{$area->id}}">{{$area->name}}</option>
                @endforeach
               </select>
          </div> 
          
          <div class="mb-3">
            <label class="form-label">Params Used in this Water Meter</label>
            <div>
              @if($customParams)
                @foreach($customParams as $param)
                    <div class="form-check form-check-inline" style="width:200px">
                        <input type="checkbox" name="params[]" @if(isset($param) && isset($param->name) && in_array($param->name,$water_meterParams??[])) checked @endif  value="{{ $param->name}}" class="form-check-input" id="{{$param->name}}">
                        <label class="form-check-label" for="{{ $param->name }}">
                            {{ $param->name }}
                        </label>
                    </div>
              @endforeach
              @endif
            </div>
          </div>
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush