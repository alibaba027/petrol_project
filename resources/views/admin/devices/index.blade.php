@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />

@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">All Devices</li>
    </ol>
  </nav>

  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">DEVICES
            @if(Auth::user()->can('Add Device'))
            <a href="/admin/device/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Device</a>
            @endif
          </h6>
          <div class="table-responsive" style="width: 100%;">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>DEVICE ID</th>
                    <th>USER</th>
                    <th>TYPE</th>
                    <th>AREA</th>
                    <th>Alarm</th>
                    <th>MAX POINT</th>
                    <th>VIEW DATA</th>
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($rows as $key=>$value)
                  <tr id="itemRow_{{ $value->id }}">
                    <th>{{ $key+1 }}</th>
                    <td>{{ $value->device_id }}</td>
                    <td>{{ $value->user->name }}</td>
                    <td>{{ $value->type }}</td>
                    <td>{{ $value->area->name??'' }}</td>
                    <td>@if($value->enable_alarm)<span class="badge bg-success">YES</span> @else <span class="badge bg-danger">NO</span> @endif</td>
                    <td>{{ $value->max_point_psi??'' }}</td>
                    <td><a href="/admin/device/data/{{ $value->id }}">View Data</a></td>
                    <td>
                        @if(Auth::user()->can('Edit Device'))
                        <a href="/admin/device/edit/{{ $value->id }}"><i class="icon feather icon-edit"></i></a>&nbsp;&nbsp;
                        @endif
                        @if(Auth::user()->can('Delete Device'))
                        <i class="icon feather icon-trash-2 deleteModel" data-url="/admin/device/delete/{{ $value->id }}" data-bs-toggle="modal" data-bs-target="#deleteItem"></i>
                        @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>

@endpush
