@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Device</a></li>
      <li class="breadcrumb-item active" aria-current="page">Add/Edit Device</li>
    </ol>
  </nav>

  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($device->id)) action="{{ url('/admin/device/update') }}" @else action="{{ url('/admin/device/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($device->id)) Edit Device @else Add Device @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($device->id))
                <input type="hidden" name="id" value="{{ $device->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Device ID</label>
                <input type="text" class="form-control" name="device_id" value="{{ $device->device_id??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Device ID" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Device Type</label>
               <select class="form-control" name="type" required>
                <option value="">--select type--</option>
                <option @if(isset($device) && $device->type=='LPM-100') selected @endif value="LPM-100">LPM-100 V1</option>
                <option @if(isset($device) && $device->type=='LPFR-100') selected @endif value="LPFR-100">LPFR-100 V1</option>
               </select>
            </div>
            <div class="mb-3">
                <label class="form-label">User</label>
               <select class="form-control" name="user_id" required>
                <option value="">--select user--</option>
                @foreach($users as $user)
                <option @if(isset($device) && $device->user->id==$user->id) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
               </select>
            </div>
            <div class="mb-3">
              <label class="form-label">Area</label>
             <select class="form-control" name="area_id" required>
              <option value="">--select area--</option>
              @foreach($areas as $area)
              <option @if(isset($device) && $device->area_id==$area->id) selected @endif value="{{$area->id}}">{{$area->name}}</option>
              @endforeach
             </select>
          </div>
          <div class="mb-3">
            {{-- <label for="exampleInputUsername1" class="form-label">Enable Alarm</label> --}}
            <div class="form-check form-check-inline" style="width:200px">
              <input type="checkbox" @if(isset($device) && $device->enable_alarm) checked @endif name="enable_alarm"  value="1" class="form-check-input">
              <label class="form-check-label" for="">
                Enable Alarm
              </label>
          </div>
        </div>
        <div class="mb-3">
          <label for="exampleInputUsername1" class="form-label">Max Point</label>
          <input type="number" class="form-control" name="max_point_psi" @if(isset($device)) value="{{ $device->max_point_psi }}" @endif autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Max Point" required>
        </div>
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush
