@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#"> Setting</a></li>
      <li class="breadcrumb-item active" aria-current="page">Email Server</li>
    </ol>
  </nav>

  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post"  action="{{ url('/admin/setting/email') }}" >
                @csrf
          <h6 class="card-title">Email Server Config
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                <label for="exampleInputUsername1" class="form-label">Email Driver</label>
                <input type="text" class="form-control" name="email_driver" value="{{ $setting->email_driver??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Email Driver" required>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="mb-3">
                <label for="exampleInputUsername1" class="form-label">Email Host</label>
                <input type="text" class="form-control" name="email_host" value="{{ $setting->email_host??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Email Host" required>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="mb-3">
                <label for="exampleInputUsername1" class="form-label">Email Port</label>
                <input type="text" class="form-control" name="email_port" value="{{ $setting->email_port??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Email Port" required>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="mb-3">
                <label for="exampleInputUsername1" class="form-label">Email Encryption</label>
                <input type="text" class="form-control" name="email_encryption" value="{{ $setting->email_encryption??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="mail Encryption" required>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="mb-3">
                <label for="exampleInputUsername1" class="form-label">Email Username</label>
                <input type="text" class="form-control" name="email_username" value="{{ $setting->email_username??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Email Username" required>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="mb-3">
                <label for="exampleInputUsername1" class="form-label">Email Password</label>
                <input type="password" class="form-control" name="email_password" value="{{ $setting->email_password??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Email Password" required>
            </div>
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush
