@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
  
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">All Master Meters</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Master Meters
           {{-- @if(Auth::user()->can('Add Well')) --}}
            <a href="/admin/master_meter/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Master Meter</a>
            {{-- @endif --}}
          </h6>
          <div class="table-responsive" style="width: 100%;">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>NAME</th>
                    <th>AREA</th>
                    @foreach($custom_params as $param)
                    <th>{{ $param }}</th>
                    @endforeach
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($rows as $key=>$value)
                  <tr id="itemRow_{{ $value->id }}">
                    <th>{{ $key+1 }}</th>
                    <td><a href="/admin/master_meter/add/data/{{ $value->id }}">{{ $value->name }}</a></td>
                    <td>{{ $value->area->name??'' }}</td>
                    @php
                    $values=(array)json_decode($value->params);
                    @endphp
                    @foreach($custom_params as $param)
                      <td>{{ $values[$param]??'' }}</td>
                    @endforeach 
                    <td>
                      {{-- @if(Auth::user()->can('Edit Well')) --}}
                        <a href="/admin/master_meter/edit/{{ $value->id }}"><i class="icon feather icon-edit"></i></a>&nbsp;&nbsp; 
                       {{-- @endif --}}
                        {{-- @if(Auth::user()->can('Delete Well')) --}}
                        <i class="icon feather icon-trash-2 deleteModel" data-url="/admin/master_meter/delete/{{ $value->id }}" data-bs-toggle="modal" data-bs-target="#deleteItem"></i> 
                    {{-- @endif --}}
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
  
@endpush