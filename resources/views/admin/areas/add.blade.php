@extends('layout.master')

@push('plugin-styles')
<link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Area</a></li>
      <li class="breadcrumb-item active" aria-current="page">Add/Edit Area</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($area->id)) action="{{ url('/admin/area/update') }}" @else action="{{ url('/admin/area/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($area->id)) Edit Area @else Add Area @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($area->id)) 
                <input type="hidden" name="id" value="{{ $area->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $area->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name">
            </div> 
            @if(in_array('Super Admin',Auth::user()->getRoleNames()->toArray()))
            <div class="mb-3">
                <label class="form-label">User</label>
               <select class="form-control" name="user_id" required>
                <option value="">--select user--</option>
                @foreach($users as $user)
                <option @if(isset($area) && $area->user && $area->user->id==$user->id) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
               </select>
            </div>
            @endif
           
            @if(in_array('Corporate Admin',Auth::user()->getRoleNames()->toArray()) && isset($supvisors_users))
            <div class="mb-3">
              <label class="form-label">Supervisor</label>
              <select class="js-example-basic-multiple form-select" multiple="multiple" data-width="100%" name="supervisor_ids[]" required>
             
              <option value="">--select--</option>
              @foreach($supvisors_users as $user)
              <option @if(isset($area) && $area->supervisors && count($area->supervisors->pluck('id')) && in_array($user->id,$area->supervisors->pluck('id')->toArray())) selected @endif value="{{$user->id}}">{{$user->name}}</option>
              @endforeach
             </select>
          </div>
          <div class="mb-3">
            <label class="form-label">Field Tech</label>
            <select class="js-example-basic-multiple form-select" multiple="multiple" data-width="100%" name="feild_tech_ids[]" required>
           
            <option value="">--select--</option>
            @foreach($field_tech_users as $user)
            <option  @if(isset($area) && $area->fieldTechs && count($area->fieldTechs->pluck('id')) && in_array($user->id,$area->fieldTechs->pluck('id')->toArray())) selected @endif value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
           </select>
        </div>
          
          @endif
            <div class="mb-3">
                <label class="form-label">Params Used in this area</label>
                <div>
                  @if($customParams)
                    @foreach($customParams as $param)
                        <div class="form-check form-check-inline" style="width:200px">
                            <input type="checkbox" name="params[]" @if(isset($param) && isset($param->name) && in_array($param->name,$areaParams??[])) checked @endif  value="{{ $param->name}}" class="form-check-input" id="{{$param->name}}">
                            <label class="form-check-label" for="{{ $param->name }}">
                                {{ $param->name }}
                            </label>
                        </div>
                  @endforeach
                  @endif
                </div>
              </div> 
               
          </div>
        
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
<script src="{{ asset('assets/plugins/inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>

@endpush

@push('custom-scripts')

  <script src="{{ asset('assets/js/select2.js') }}"></script>
  <script src="{{ asset('assets/js/typeahead.js') }}"></script>
  <script src="{{ asset('assets/js/tags-input.js') }}"></script>
  <script src="{{ asset('assets/js/dropify.js') }}"></script>
 
@endpush