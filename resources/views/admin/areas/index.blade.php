@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
  
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Areas</a></li>
      <li class="breadcrumb-item active" aria-current="page">All Areas</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Areas
           @if(Auth::user()->can('Add Area'))
            <a href="/admin/area/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Area</a>
            @endif
          </h6>
          <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>AREA NAME</th>
                    <th>USER</th>
                    <th>SUPERVISOR</th>
                    <th>FIELD TECH</th>
                    <th>AREA PARAMS</th>
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($rows as $key=>$value)
                  <tr id="itemRow_{{ $value->id }}">
                    <th>{{ $key+1 }}</th>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->user->name??'' }}</td>
                    <td>
                      @if($value->supervisors)
                        @foreach($value->supervisors as $key=>$s)
                        {{ $s->name }} @if($key+1!=count($value->supervisors)),@endif
                        @endforeach
                      @endif
                    </td>
                    <td>
                      @if($value->fieldTechs)
                        @foreach($value->fieldTechs as $key=>$f)
                        {{ $f->name }} @if($key+1!=count($value->fieldTechs)),@endif
                        @endforeach
                      @endif
                    </td>
                    @php
                    $values=(array)json_decode($value->params);
                    @endphp
                    <td>
                    @foreach($values as $key=>$param)
                      {{ $param }} @if($key!=count($values)-1),@endif
                    @endforeach 
                    </td>    
                    <td>
                      @if(Auth::user()->can('Edit Area'))
                        <a href="/admin/area/edit/{{ $value->id }}"><i class="icon feather icon-edit"></i></a>&nbsp;&nbsp; 
                       @endif
                        @if(Auth::user()->can('Delete Area'))
                        <i class="icon feather icon-trash-2 deleteModel" data-url="/admin/area/delete/{{ $value->id }}" data-bs-toggle="modal" data-bs-target="#deleteItem"></i> 
                    @endif
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
  
@endpush