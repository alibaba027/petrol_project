@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Tank</a></li>
      <li class="breadcrumb-item active" aria-current="page">Add/Edit Tank</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($tank->id)) action="{{ url('/admin/tank/update') }}" @else action="{{ url('/admin/tank/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($tank->id)) Edit Tank @else Add Tank @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($tank->id)) 
                <input type="hidden" name="id" value="{{ $tank->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $tank->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name">
            </div>
            <div class="mb-3">
              <label for="exampleInputUsername1" class="form-label">Area</label>
              <select class="form-control" name="area_id" required>
                <option value="">--select user--</option>
                @foreach($areas as $area)
                <option @if(isset($tank) && $tank->area && $tank->area->id==$area->id) selected @endif value="{{$area->id}}">{{$area->name}}</option>
                @endforeach
               </select>
          </div> 
          
          <div class="mb-3">
            <label class="form-label">Params Used in this Tank</label>
            <div>
              @if($customParams)
                @foreach($customParams as $param)
                    <div class="form-check form-check-inline" style="width:200px">
                        <input type="checkbox" name="params[]" @if(isset($param) && isset($param->name) && in_array($param->name,$tankParams??[])) checked @endif  value="{{ $param->name}}" class="form-check-input" id="{{$param->name}}">
                        <label class="form-check-label" for="{{ $param->name }}">
                            {{ $param->name }}
                        </label>
                    </div>
              @endforeach
              @endif
            </div>
          </div>
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush