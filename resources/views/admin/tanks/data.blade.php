@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Tank</a></li>
      <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($tank->id)) action="{{ url('/admin/tank/update/data') }}" @else action="{{ url('/admin/tank/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($tank->id)) Edit Data @else Add Data @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($tank->id)) 
                <input type="hidden" name="id" value="{{ $tank->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $tank->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name" readonly>
            </div>  
            @php 
            $param_value='';
            $param_value=json_decode($tank->params??'');
            $param_value=(array)$param_value;
            @endphp
            @foreach($param_value as $key=>$param)
            <div class="mb-3">
                <label class="form-label">{{ $key }}</label>
                <input type="text" class="form-control" name="param[{{$key}}]" value="{{ $param??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="{{ $key  }}">
            </div>   
             @endforeach
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush