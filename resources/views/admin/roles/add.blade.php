@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Roles</a></li>
      <li class="breadcrumb-item active" aria-current="page">All Roles</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
            <form method="post" @if(isset($role->id)) action="{{ url('/admin/role/update') }}" @else action="{{ url('/admin/role/store') }}" @endif>
                @csrf
          <h6 class="card-title">@if(isset($role->id)) Edit Role @else Add Role @endif
          </h6>
          <div class="col-lg-12">
            <div class="mb-3">
                @if(isset($role->id)) 
                <input type="hidden" name="id" value="{{ $role->id }}">
                @endif
                <label for="exampleInputUsername1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $role->name??'' }}" autofocus id="exampleInputUsername1" autocomplete="off" placeholder="Name">
            </div> 
            <div class="mb-3">
                <label class="form-label">Permissions</label>
                <div>
                    @php
                    $role_permission=[];
                    if(isset($role->id) && $role->permissions && count($role->permissions)>0){
                        $role_permission=$role->permissions->pluck('name')->toArray();
                    }
                    @endphp
                    @foreach($permissions as $permission)
                        <div class="form-check form-check-inline" style="width:200px">
                            <input type="checkbox" name="permissions[]" @if(in_array($permission->name,$role_permission)) checked @endif value="{{ $permission->name }}" class="form-check-input" id="{{ $permission->name }}">
                            <label class="form-check-label" for="{{ $permission->name }}">
                                {{ $permission->name }}
                            </label>
                        </div>
                  @endforeach
                </div>
              </div>  
          </div>
          <input class="btn btn-primary" type="submit" value="Submit" style="float: right">
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush