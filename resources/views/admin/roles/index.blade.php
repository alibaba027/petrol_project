@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
  
@endpush

@section('content')
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Roles</a></li>
      <li class="breadcrumb-item active" aria-current="page">All Roles</li>
    </ol>
  </nav>
  
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Roles
            @if(Auth::user()->can('Add Role'))<a href="/admin/role/create" class="btn btn-sm btn-inverse-secondary" style="float: right">Add Role</a>@endif
          </h6>
          <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ROLE NAME</th>
                    <th>PERMISSIONS</th>
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($rows as $key=>$value)
                  <tr id="itemRow_{{ $value->id }}">
                    <th>{{ $key+1 }}</th>
                    <td>{{ $value->name }}</td>
                    <td>@foreach($value->permissions as $uPer)<span class="badge bg-success">{{ $uPer->name }}</span> @endforeach</td>     
                    <td>
                      @if(Auth::user()->can('Edit Role'))  
                      <a href="/admin/role/edit/{{ $value->id }}"><i class="icon feather icon-edit"></i></a>&nbsp;&nbsp; 
                      @endif
                      @if(Auth::user()->can('Delete Role'))
                       @if($value->name!='Super Admin') <i class="icon feather icon-trash-2 deleteModel" data-url="/admin/role/delete/{{ $value->id }}" data-bs-toggle="modal" data-bs-target="#deleteItem"></i> @endif
                        @endif
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/fullcalendar/index.global.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
  
@endpush